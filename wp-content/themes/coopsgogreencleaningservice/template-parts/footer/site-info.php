<div class="info">
	<div class="copyright has-dark-gray-color">
		<?php
		$site_info = get_bloginfo('description') . ' - ' . get_bloginfo('name') . ' &copy; ' . date('Y');

		if (get_theme_mod('copyright')) :
			echo get_theme_mod('copyright');
		else :
			echo $site_info;
		endif;
		?>
	</div>
	<?php if (get_theme_mod('footer_silver')) : ?>
		<div class="silver has-dark-gray-color">
			<img src="<?php echo get_theme_mod('footer_silver'); ?>)" alt="" class="company-logo" />
			<a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">
				Web Design
			</a>
			Done by
			<a href="https://silverconnectwebdesign.com" rel="external" target="_blank">
				Silver Connect Web Design
			</a>
		</div>
	<?php endif; ?>
</div>