<?php
/**
 * Displays the page bottom
 *
 * @package WordPress
 * @subpackage Coops_Go_Green_Cleaning_Service
 * @since 1.0.0
 */

if ( is_active_sidebar( 'site-page-bottom' ) ) : ?>

	<div class="site-page-bottom">
		<div class="container">
			<?php dynamic_sidebar( 'site-page-bottom' ); ?>
		</div>
	</div><!-- .widget-area -->

<?php endif; ?>
