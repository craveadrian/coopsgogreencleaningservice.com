<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Coops_Go_Green_Cleaning_Service
 * @since 1.0.0
 */

?>

</div><!-- #content -->
<?php get_template_part('template-parts/footer/site', 'page-bottom'); ?>
<footer id="colophon" class="site-footer">
	<div class="site-info container">
		<div class="footer-info">
			<?php get_template_part('template-parts/footer/site', 'info'); ?>
		</div>

		<?php if (checkoption('footer_logo')) : ?>
			<div class="footer-logo">
				<a href="<?php echo site_url(); ?>"> <?php echo do_shortcode('[coopsgogreencleaningservice_option var="footer_logo" type="image" text="Footer Logo"]'); ?> </a>
			</div>
		<?php endif; ?>
		<div class="footer-sm">
			<?php get_template_part('template-parts/navigation/navigation', 'social'); ?>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>