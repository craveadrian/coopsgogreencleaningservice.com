<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'coopsgogreencleaningservice' ),
		'singular_name'			=> __( 'Fence', 'coopsgogreencleaningservice' ),
		'add_new'				=> __( 'New Fence', 'coopsgogreencleaningservice' ),
		'add_new_item'			=> __( 'Add New Fence', 'coopsgogreencleaningservice' ),
		'edit_item'				=> __( 'Edit Fence', 'coopsgogreencleaningservice' ),
		'new_item'				=> __( 'New Fence', 'coopsgogreencleaningservice' ),
		'view_item'				=> __( 'View Fence', 'coopsgogreencleaningservice' ),
		'search_items'			=> __( 'Search Fences', 'coopsgogreencleaningservice' ),
		'not_found'				=>  __( 'No Fences Found', 'coopsgogreencleaningservice' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'coopsgogreencleaningservice' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'coopsgogreencleaningservice' ),
		'singular_name'		=> __( 'Category', 'coopsgogreencleaningservice' ),
		'search_items'		=> __( 'Search Categories', 'coopsgogreencleaningservice' ),
		'all_items'			=> __( 'All Categories', 'coopsgogreencleaningservice' ),
		'edit_item'			=> __( 'Edit Category', 'coopsgogreencleaningservice' ),
		'update_item'		=> __( 'Update Category', 'coopsgogreencleaningservice' ),
		'add_new_item'		=> __( 'Add New Category', 'coopsgogreencleaningservice' ),
		'new_item_name'		=> __( 'New Category Name', 'coopsgogreencleaningservice' ),
		'menu_name'			=> __( 'Categories', 'coopsgogreencleaningservice' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );