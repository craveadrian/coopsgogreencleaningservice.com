<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'betascxs_coopsgogreencleaningservice' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'BV9k`d$7U6%]3TI*YN!G*4hMI.&Wr]6$nJ-qUy-F:c<G{uA9VQ<c/pggRpH&c=| ' );
define( 'SECURE_AUTH_KEY',  '5q$x&egs<Vzxi!Wgk=#)AzUxg(7N3(u}|m^|-rX1^V((8xQfn<#B|l`@m0`Zc1OZ' );
define( 'LOGGED_IN_KEY',    'CY;hZoEi-?YW>px*J~L%7#)ove{qN Y4^1Op=~8S%_Si+$fsWD9 w{HUqKZmXxeU' );
define( 'NONCE_KEY',        '#>-e:i<~<p[_LI#&jo$0ZuAD|]K7#+Xjt%H/%3##VHP:m8J3m66<g>cB+mA>${Ui' );
define( 'AUTH_SALT',        ')W4SPF6S)jbBbMw9,wp}]c7*MC#^v5)+1u}wLvaPYR3n4*9Q8N(m:cZl95WlxdF6' );
define( 'SECURE_AUTH_SALT', 'Sm8qzs!(9dCrP7Wo&V=bO|Lz~81M0D<+QIXAu|8B9_8C;[+^%dI..tYA10GpvkHd' );
define( 'LOGGED_IN_SALT',   'J|$6yuP6BjF*,a1VUg5yOQ|.x`ddG}#K$o<Jv{+6hu1fr*jA=`zQKKldoU>Q/_]r' );
define( 'NONCE_SALT',       ':ob={K!:6E2z0F-&}!1RgS@Uq~6?Hc~=7bF^oj8kojP/b^pLr3y6n@dp=Xmn0t9-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'betascxs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
